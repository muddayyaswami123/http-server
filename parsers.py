#parsing the request message to handle different requests

def parse_request(message):
    
    reqStr = message.strip()
    splitBodyFromHeader = reqStr.split("\r\n\r\n".encode(), 1)

    reqMsgAndHeaders = splitBodyFromHeader[0].decode('utf-8')
    requestBody = ''
    #Sometimes request body can be empty, hence check length of message split
    if(len(splitBodyFromHeader)>1):
        requestBody = splitBodyFromHeader[1]

    headerData = reqMsgAndHeaders.strip().split('\r\n')
    reqMsg = headerData[0].split(" ")
    reqHeaders = headerData[1:]

    reqMsgDict = dict()
    reqMsgDict['method'] = reqMsg[0]
    reqMsgDict['req_uri'] = reqMsg[1]
    reqMsgDict['httpVersion'] = reqMsg[2]

    reqHeaderdict = dict()
    for header in reqHeaders:
        seperator = header.split(":", 1)
        hType = seperator[0]
        value = seperator[1]
        #add this key value pair in header maintaining dictionary
        reqHeaderdict[hType.strip().lower()] = value.strip()
    
    newParsedMessage = {
        'reqMsgDict': reqMsgDict,
        'reqHeaderdict': reqHeaderdict,
        'requestBody': requestBody
    }

    return newParsedMessage
    


