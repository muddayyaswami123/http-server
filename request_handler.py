#this file handles different requests such as get, head, post, put, delete
from parsers import *
from response_helpers import *
import os
import mimetypes
import response_helpers
import config
from datetime import datetime
from time import mktime
import time
import gzip
import zlib
import brotli
import logging
import hashlib


def method_handler(req,clientname,cAddress):
    print(req)
    newParsedMessage = parse_request(req)
    reqLine = newParsedMessage['reqMsgDict']
    url = reqLine['req_uri']
    method = newParsedMessage['reqMsgDict']['method']
    if(method=="GET"):
        response = handle_get(newParsedMessage,cAddress)
    if(method=="HEAD"):
        response = handle_head(newParsedMessage)
    if(method=="DELETE"):
        response = handle_delete(newParsedMessage)
    if(method=="PUT"):
        response = handle_put(newParsedMessage)
    if(method=="POST"):
        response = handle_post(newParsedMessage)
    print("response from method")
    print(response)
    res = parse_request(response)
    statusCode = res['reqMsgDict']['req_uri']
    logging.info('	{}	{}	{}	{}	{}\n'.format(cAddress[0], cAddress[1], reqLine,url, statusCode))
    print(response)
    return response


def handle_get(newParsedMessage,cAddress):

    req_uri = newParsedMessage['reqMsgDict']['req_uri']
    filepath = req_uri

    if(filepath == '/'):
        filepath = "/media/index.html"
    
    Headers = newParsedMessage['reqHeaderdict']

    if('host' not in Headers):
        response = handle_other_statuscodes(400)
        return response

    acceptType= Headers.get('accept', "*/*")
    accept_encoding = Headers.get('accept-encoding', "")
    print(acceptType)
    print(accept_encoding)

    path = os.getcwd()
    path+= filepath
    ext = mimetypes.guess_type(path)[0]
    print(ext)
    CT = ext
    #get content encoding based on the priority of the encoding method
    Content_encoding = get_Content_Encoding(accept_encoding)
    ce = Content_encoding
    if(Content_encoding == 'Identity'):
        ce = ""
    
    if(not Content_encoding):
        res = handle_other_statuscodes(406)
        return res

    if(os.path.isfile(path)):

        LM = datetime.fromtimestamp(mktime(time.gmtime(os.path.getmtime(path))))
        ifmod = Headers.get('if-modified-since',Format_date(datetime.fromtimestamp(0)))         
        ifunmod = Headers.get('if-unmodified-since',Format_date(datetime.utcnow()))

        ifmatch = Headers.get('if-match',"*")
        print("this is ifmatch",ifmatch)
        ifnonematch = Headers.get('if-none-match',"")
        print(ifnonematch)
        ifmatchlist = parseifmatch(ifmatch)
        ifnmatchlist = parseifmatch(ifnonematch)
        print(ifnmatchlist)
        Etag =  '{}'.format(hashlib.md5((Format_date(LM) + ce).encode()).hexdigest())
        print(Etag)
        for iftag in ifmatchlist:
            iftag = iftag.strip()
            print("this is iftag", iftag)
            print("this is etag", Etag)
            if(iftag == Etag or iftag == "*"):
                print("iftag is equal to etag")
                break
            else:
                statusCode = 412
                response = handle_Modified(statusCode,Etag,LM)
                return response
        for ifntag in ifnmatchlist:
            print(ifntag)
            if( ifntag== Etag ):
                statusCode = 304
                response =handle_Modified(statusCode,Etag,LM)
                return response
            elif ifntag== "":
                break

        #open the file and read the content
        with open(path, "rb") as f:
            File_body = f.read()

        resBody = File_body

        if(Content_encoding == 'gzip' or Content_encoding == '*'):
            resBody = gzip.compress(resBody)
        
        if(Content_encoding == 'br'):
            resBody = brotli.compress(resBody)
        
        if(Content_encoding == 'deflate'):
            resBody = zlib.compress(resBody)
        
        length = str(len(resBody))
        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"200", 'phrase':"OK"}
        resheaders = {
            'content-type':CT,
            'connection': "keep-alive",
            'content-length': length,
            'server': config.server,
            'Date': Format_date(datetime.utcnow()),
            'Last-modified': Format_date(LM),
            'Content-Language': "en",
            'Etag': Etag,
            'Set-Cookie': Bulid_cookie(cAddress)

        }
        if(Content_encoding != 'Identity'):
            resheaders.__setitem__('Content-Encoding', Content_encoding)
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': resBody
        }
        response = build_response(resDict)
        print(response)
        return response
    else:
        path2 = os.getcwd()
        path2 += "/media/FNF.html"
        with open(path2,"rb") as f:
            resBody = f.read()

        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"404", 'phrase':"not found"}
        resheaders = {
            'connection': "keep-alive",
            'server': config.server,
            'Date': Format_date(datetime.utcnow())
        }
        
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': "".encode()  
        }
        response = build_response(resDict)
        print(response)
        return response



def handle_head(newParsedMessage):

    req_uri = newParsedMessage['reqMsgDict']['req_uri']
    filepath = req_uri

    if(filepath == '/'):
        filepath = "/media/index.html"
    
    Headers = newParsedMessage['reqHeaderdict']

    if('host' not in Headers):
        response = handle_other_statuscodes(400)
        return response

    acceptType= Headers.get('accept', "*/*")
    accept_encoding = Headers.get('accept-encoding', "")
    print(acceptType)
    print(accept_encoding)

    path = os.getcwd()
    path+= filepath
    ext = mimetypes.guess_type(path)[0]
    print(ext)
    CT = ext

    if(ext == 'None'):
       CT = get_Accept(acceptType)

    #get content encoding based on the priority of the encoding method
    Content_encoding = get_Content_Encoding(accept_encoding)
    ce = Content_encoding
    if(Content_encoding == 'Identity'):
        ce = ""
    
    if(not Content_encoding):
        res = handle_other_statuscodes(406)
        return res

    if(os.path.isfile(path)):

        LM = datetime.fromtimestamp(mktime(time.gmtime(os.path.getmtime(path))))
        ifmod = Headers.get('if-modified-since',Format_date(datetime.fromtimestamp(0)))         
        ifunmod = Headers.get('if-unmodified-since',Format_date(datetime.utcnow()))

        ifmatch = Headers.get('if-match',"*")
        ifnonematch = Headers.get('if-none-match',"")
        print(ifnonematch)
        ifmatchlist = parseifmatch(ifmatch)
        ifnmatchlist = parseifmatch(ifnonematch)
        print(ifnmatchlist)
        Etag =  '{}'.format(hashlib.md5((Format_date(LM) + ce).encode()).hexdigest())
        print(Etag)
        for iftag in ifmatchlist:
            if iftag == Etag or iftag == "*":
                break
            else:
                statusCode = 412
                response = handle_Modified(statusCode,Etag,LM)
                return response
        for ifntag in ifnmatchlist:
            print(ifntag)
            if( ifntag== Etag ):
                print
                statusCode = 304
                response =handle_Modified(statusCode,Etag,LM)
                return response
            elif ifntag== "":
                break

        #open the file and read the content
        with open(path, "rb") as f:
            File_body = f.read()

        resBody = File_body

        if(Content_encoding == 'gzip' or Content_encoding == '*'):
            resBody = gzip.compress(resBody)
        
        if(Content_encoding == 'br'):
            resBody = brotli.compress(resBody)
        
        if(Content_encoding == 'deflate'):
            resBody = zlib.compress(resBody)
        
        length = str(len(resBody))
        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"200", 'phrase':"OK"}
        resheaders = {
            'content-type':CT,
            'connection': "keep-alive",
            'content-length': length,
            'server': config.server,
            'Date': Format_date(datetime.utcnow()),
            'Last-modified': Format_date(LM),
            'Content-Language': "en"

        }
        if(Content_encoding != 'Identity'):
            resheaders.__setitem__('Content-Encoding', Content_encoding)
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': "".encode()
        }
        response = build_response(resDict)
        print(response)
        return response
    else:
        path2 = os.getcwd()
        path2 += "/media/FNF.html"
        with open(path2,"rb") as f:
            resBody = f.read()

        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"404", 'phrase':"not found"}
        resheaders = {
            'connection': "keep-alive",
            'server': config.server,
            'Date': Format_date(datetime.utcnow())
        }
        
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': "".encode()
        }
        response = build_response(resDict)
        print(response)
        return response





def handle_delete(newParsedMessage):
    req_uri = newParsedMessage['reqMsgDict']['req_uri']
    path = os.getcwd()
    path += req_uri
    if(os.path.isfile(path)):
        os.remove(path)
        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"200", 'phrase':"OK"}

        resheaders = {
            'connection': "keep-alive",
            'server': config.server,
            'Date': Format_date(datetime.utcnow())

        }
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': "File deleted successfully...".encode()
        }
        response = build_response(resDict)
        return response
    else:
        path2 = os.getcwd()
        path2 += "/media/FNF.html"
        with open(path2,"rb") as f:
            resBody = f.read()
        f.close()
        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"404", 'phrase':"not found"}
        resheaders = {
            'connection': "keep-alive",
            'server': config.server,
            'Date': Format_date(datetime.utcnow()),
            
        }
        
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': resBody
        }
        response = build_response(resDict)
        return response

    


def handle_put(newParsedMessage):
    req_uri = newParsedMessage['reqMsgDict']['req_uri']
    path = os.getcwd()
    path += req_uri
    reqBody = newParsedMessage['requestBody']
    if(not reqBody):
        response = handle_other_statuscodes(204)
        return response
    reqBody = reqBody.decode()
    if(os.path.isfile(path)):

        with open(path, "w") as f:
            f.write(reqBody)

        response = handle_other_statuscodes(200)
        return response
        # responseLine = {'httpver': "HTTP/1.1", 'statusCode':"200", 'phrase':"OK"}
        # resheaders = {
        #     'connection': "keep-alive",
        #     'server': config.server,
        #     'Date': Format_date(datetime.utcnow()),
        #     'Content-Language': "en"
        # }
        # resDict = {
        #     'responseLine': responseLine,
        #     'resheaders': resheaders,
        #     'responseBody': "".encode()
        # }
        # response = build_response(resDict)
        # print(response)
        # return response
    else:
        #creating for first time
        with open(path, "w") as f:
            f.write(reqBody)

        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"201", 'phrase':"Created"}
        resheaders = {
            'connection': "keep-alive",
            'server': config.server,
            'Date': Format_date(datetime.utcnow()),
            'Content-Language': "en"
        }
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': "".encode()
        }
        response = build_response(resDict)
        print(response)
        return response



def handle_post(newParsedMessage):

    reqBody = newParsedMessage['requestBody']
    if(not reqBody):
        response = handle_other_statuscodes(204)
        return response
    reqBody = reqBody.decode()
    path = os.getcwd()
    post = config.postdata
    path += post
    if(os.path.isfile(path)):

        with open(path, "a") as f:
            f.write(reqBody)
            f.write("\n")
 
        responseLine = {'httpver':"HTTP/1.1", 'statusCode':"200", 'phrase':"OK"}
        resheaders = {
            'connection': "keep-alive",
            'server': config.server,
            'Date': Format_date(datetime.utcnow())
        }
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': "".encode()
        }
        response = build_response(resDict)
        print(response)
        return response
    else:
        #creating for first time
        with open(path, "a") as f:
            f.write(reqBody)

        responseLine = {'httpver': "HTTP/1.1", 'statusCode':"201", 'phrase':"Created"}
        resheaders = {
            'connection': "keep-alive",
            'server': config.server,
            'Date': Format_date(datetime.utcnow()),
            'Content-Language': "en"
        }
        resDict = {
            'responseLine': responseLine,
            'resheaders': resheaders,
            'responseBody': "".encode()
        }
        response = build_response(resDict)
        print(response)
        return response

    
def handle_other_statuscodes(statusCode):

    responseLine = {'httpver': "HTTP/1.1", 'statusCode':str(statusCode), 'phrase':givePhrase(statusCode)}
    resheaders = {
        'connection': "keep-alive",
        'server': config.server,
        'Date': Format_date(datetime.utcnow())
    }
    
    resDict = {
        'responseLine': responseLine,
        'resheaders': resheaders,
        'responseBody': "".encode()
    }
    response = build_response(resDict)
    print(response)
    return response
    

    
def handle_Modified(statusCode,Etag,LM):

    responseLine = {'httpver': "HTTP/1.1", 'statusCode':str(statusCode), 'phrase':givePhrase(statusCode)}
    resheaders = {
        'connection': "keep-alive",
        'server': config.server,
        'Date': Format_date(datetime.utcnow()),
        'Last-modified': Format_date(LM),
        'Etag': Etag
        
    }
    
    resDict = {
        'responseLine': responseLine,
        'resheaders': resheaders,
        'responseBody': "".encode()
    }
    response = build_response(resDict)
    print(response)
    return response
    
