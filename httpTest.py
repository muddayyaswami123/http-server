import socket
import threading
import sys
import parsers
import response_helpers

def receive():

    try:              
        message= client.recv(4096)
        parsed_msg = parsers.parse_request(message)

    except Exception as ex:
        print('\n\nThe test case failed', ex)
    finally:
        return parsed_msg
    



def Test_01():
    #get test_01 normal get
    req= "GET / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    if(status=='200'):
        print('\nGET TEST-01 PASS', status, Phrase)
    else:
        print('Test case failed..')




def Test_02():
    #get test_02 412 precondition failed
    req= "GET / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\nIf-Modified-Since: Sat, 23 Oct 2021 12:29:51\r\nIf-Match: 14ff13d968c053380132979\r\nIf-None-Match: 14ff13d968c053380132970415af0679\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    if(status=='412'):    
        print('\nGET TEST-02 PASS', status, Phrase)



def Test_03():
    #get test_03 304 Not Modiifed
    req= "GET / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\nIf-Modified-Since: Sat, 23 Oct 2021 12:29:51\r\nIf-Match: 14ff13d968c053380132970415af0679\r\nIf-None-Match: 14ff13d968c053380132970415af0679\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nGET TEST-03 PASS', status, Phrase)



def Test_04():
    #get test_04 404 Not found
    req= "GET /xyz.js HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nGET TEST-04 PASS', status, Phrase)



def Test_05():
    #head test_01 normal head
    req= "HEAD / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nHEAD TEST-01 PASS', status, Phrase)




def Test_06():
    #head test_02 412 precondition failed
    req= "HEAD / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\nIf-Modified-Since: Sat, 23 Oct 2021 12:29:51\r\nIf-Match: 14ff13d968c053380132979\r\nIf-None-Match: 14ff13d968c053380132970415af0679\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nHEAD TEST-02 PASS', status, Phrase)



def Test_07():
    #head test_03 304 Not Modiifed
    req= "HEAD / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\nIf-Modified-Since: Sat, 23 Oct 2021 12:29:51\r\nIf-Match: 14ff13d968c053380132970415af0679\r\nIf-None-Match: 14ff13d968c053380132970415af0679\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nHEAD TEST-03 PASS', status, Phrase)



def Test_08():
    #head test_04 404 Not found
    req= "HEAD /xyz.js HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nHEAD TEST-04 PASS', status, Phrase)



def Test_09():
    #post test_01 normal post 200 ok
    req= "POST /postdata.txt HTTP/1.1\r\nHost: 127.0.0.1:4540\r\nUser-Agent: python-requests/2.22.0\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\nHello this new data"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nPOST TEST-01 PASS', status, Phrase)



def Test_10():
    #post test_01 204 No content
    req= "POST /postdata.txt HTTP/1.1\r\nHost: 127.0.0.1:4540\r\nUser-Agent: python-requests/2.22.0\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nPOST TEST-02 PASS', status, Phrase)



def Test_11():
    #PUT test_01 204 No Content
    req= "PUT /PutFiles/third.txt HTTP/1.1\r\nHost: 127.0.0.1:1234\r\nUser-Agent: python-requests/2.22.0\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nPUT TEST-01 PASS', status, Phrase)



def Test_12():
    #Put test_02 201 created
    req= "PUT /PutFiles/third.txt HTTP/1.1\r\nHost: 127.0.0.1:1234\r\nUser-Agent: python-requests/2.22.0\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n put this in put"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nPUT TEST-02 PASS', status, Phrase)


def Test_13():
    #Put test_03 200 ok
    req= "PUT /PutFiles/third.txt HTTP/1.1\r\nHost: 127.0.0.1:1234\r\nUser-Agent: python-requests/2.22.0\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n put this in put for 2nd time"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nPUT TEST-03 PASS', status, Phrase)



def Test_14():
    #delete test_01 200 ok
    req= "DELETE /PutFiles/third.txt HTTP/1.1\r\nHost: 127.0.0.1:1321\r\nUser-Agent: python-requests/2.22.0\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nDELETE TEST-01 PASS', status, Phrase)


def Test_15():
    #delete test_03 404 Not Found
    req= "DELETE /PutFiles/muddayya.txt HTTP/1.1\r\nHost: 127.0.0.1:1321\r\nUser-Agent: python-requests/2.22.0\r\nAccept-Encoding: gzip, deflate\r\nAccept: */*\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nDELETE TEST-02 PASS', status, Phrase)



def Test_16():
    #test 400 status code
    req= "GET / HTTP/1.1\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print('\nGET 400 TEST-CASE PASS', status, Phrase)


#Multithreding test

#Get


ThreadNum = 8

def Test_17():
    try:
        count =0    
        req_threads = []
        for i in range(ThreadNum):
            t = threading.Thread(target=Mulit_test)
            req_threads.append(t)
            t.start()
        for thread in req_threads:
            thread.join()
        print("MultiThreading request worked")
    except Exception as ex:
        print("Something is wrong", ex)

    finally:
        return



def Test_18():
    try:
        count =0    
        req_threads = []
        for i in range(ThreadNum):
            t = threading.Thread(target=Mulit_test2)
            req_threads.append(t)
            t.start()
        for thread in req_threads:
            thread.join()
        print("MultiThreading request worked")
    except Exception as ex:
        print("Something is wrong", ex)

    finally:
        return

def Mulit_test():
    #get test_01 normal get
    req= "GET / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print(status)
    

def Mulit_test2():
    #get test_01 normal get
    req= "Post / HTTP/1.1\r\nHost: 127.0.0.1:4538\r\nUser-Agent: python-requests/2.22.0\r\naccept-Encoding: gzip;q=1.0,br;q=0.7\r\nAccept: */*\r\nConnection: keep-alive\r\n\r\n post thus"
    client.send(req.encode('utf-8'))
    parsed_msg = receive()
    status = parsed_msg['reqMsgDict']['req_uri']
    Phrase = response_helpers.givePhrase(status)
    print(status)





if __name__ == "__main__":

    host = '127.0.0.1'
    port = int(sys.argv[1])
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((host, port))
    Test_01()
    Test_02()
    Test_03()
    Test_04()
    Test_05()
    Test_06()
    Test_07()
    Test_08()
    Test_09()
    Test_10()
    Test_11()
    Test_12()
    Test_13()
    Test_14()
    Test_15()
    Test_16()
    Test_17()
    Test_18()



