import socket
import os
import sys
import threading
from request_handler import *
import logging

class tcpserver():

    def __init__(self,hostname, port):
        self.hostname = hostname
        self.port = port
        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serverSocket.bind((hostname,port))
        self.serverSocket.listen()
        print("server is listening..!")

    def accept(self):
        clientname, cAddress = self.serverSocket.accept()
        return clientname,cAddress
    def receive(self, clientname, dataSize):
        return clientname.recv(dataSize)
        
    def send(self, clientname, response):
        clientname.sendall(response)
	
	# def close(self, clientname):
	# 	clientname.close()




class httpServer():
    def __init__(self, host, port):
        self.status = 1
        self.tcpSocket = tcpserver(host, port)

    
    def server_requests(self):
        
        while(self.status):
            clientname, cAddress = self.tcpSocket.accept()
            serverThread = threading.Thread(target= self.server_runner, args = (clientname, cAddress ))
            serverThread.start()
            
        
    def server_runner(self, clientname, cAddress):
        while(self.status):
            req = self.tcpSocket.receive(clientname, 2048)
            if(len(req)==0):
                continue
            response = method_handler(req,clientname,cAddress)
            self.server_send(clientname, response)

    def server_send(self, clientname, response):
        self.tcpSocket.send(clientname, response)
    
    def stop():
        sys.exit()

if __name__ == "__main__":
    
    port =0
    if(len(sys.argv)>1):
        port = int(sys.argv[1])
    server = httpServer('', port)
    print(port)
    logging.basicConfig(filename = os.getcwd() + '/server.log', level = logging.INFO, format = '%(asctime)s:%(filename)s:%(message)s')
    server.server_requests()
    






