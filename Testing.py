import sys
import os
import requests
import random
import threading
import unittest

url = "http://127.0.0.1:12000" 

#test_01 for testing 200 ok status code
class test_01(unittest.TestCase):
    def runTest(self):
            
        #"Testing normal get request"
        try:
            headers = {
                'accept-Encoding': 'gzip;q=1.0,br;q=0.7',
                'Accept': '*/*',
                'Connection': 'keep-alive'
            }
            r = requests.get(url + "/", headers = headers)
            # print(f"Status : {r.status_code} {r.reason}")
            # print("Headers:", r.headers)
            if(r.status_code == 200):
                print('\nGET TEST-01 PASS', r.status_code, r.reason)
        except Exception as ex:
            print('\n\nGET TEST-01 FAIL', ex)

        finally:
            # Stop all running threads
            return


#test_02 for testing 304 Not Modified status code
class test_02(unittest.TestCase):
    def runTest(self):    
        #Testing conditional get request
        try:
            headers = {
                'accept-Encoding': 'gzip;q=1.0,br;q=0.7',
                'Accept': '*/*',
                'Connection': 'keep-alive',
                'If-Modified-Since': "Sat, 23 Oct 2021 12:29:51",
                'If-Match': "14ff13d968c053380132970415af0679",
                'If-None-Match': "14ff13d968c053380132970415af0679" 
            }
            r = requests.get(url + "/", headers = headers)
            if(r.status_code == 304 ):
                print('\nGET TEST-02 PASS', r.status_code, r.reason)
        except Exception as ex:
            print('\n\nGET TEST-02  FAIL', ex)

        finally:
            # Stop all running threads
            return



#test_01 for testing 200 ok status code
class test_03(unittest.TestCase):
    def runTest(self):
            
        #"Testing normal get request"
        try:
            headers = {
                'accept-Encoding': 'gzip;q=1.0,br;q=0.7',
                'Accept': '*/*',
                'Connection': 'keep-alive'
            }
            r = requests.head(url + "/", headers = headers)
            # print(f"Status : {r.status_code} {r.reason}")
            # print("Headers:", r.headers)
            if(r.status_code == 200):
                print('\nHEAD TEST-01 PASS', r.status_code, r.reason)
        except Exception as ex:
            print('\n\nHEAD TEST-01 FAIL', ex)

        finally:
            # Stop all running threads
            return


#test_02 for testing 304 Not Modified status code
class test_04(unittest.TestCase):
    def runTest(self):    
        #Testing conditional get request
        try:
            headers = {
                'accept-Encoding': 'gzip;q=1.0,br;q=0.7',
                'Accept': '*/*',
                'Connection': 'keep-alive',
                'If-Modified-Since': "Sat, 23 Oct 2021 12:29:51",
                'If-Match': "14ff13d968c053380132970415af0679",
                'If-None-Match': "14ff13d968c053380132970415af0679" 
            }
            r = requests.head(url + "/", headers = headers)
            if(r.status_code == 304 ):
                print('\nHEAD TEST-02 PASS', r.status_code, r.reason)
        except Exception as ex:
            print('\n\nHEAD TEST-02  FAIL', ex)

        finally:
            # Stop all running threads
            return



#test_03 for testing 412 Preconditional Failed status code
class test_05(unittest.TestCase):
    def runTest(self):    
        #Testing conditional get request
        try:
            headers = {
                'accept-Encoding': 'gzip;q=1.0,br;q=0.7',
                'Accept': '*/*',
                'Connection': 'keep-alive',
                'If-Modified-Since': "Sat, 23 Oct 2021 12:29:51",
                'If-Match': "14ff13d968c053380132979",
                'If-None-Match': "14ff13d968c053380132970415af0679" 
            }
            
            
            r = requests.head(url + "/", headers = headers)
            if(r.status_code == 412):
                print('\nHEAD TEST-03 PASS', r.status_code, r.reason)
        except Exception as ex:
            print('\n\nHEAD TEST-03  FAIL', ex)

        finally:
            # Stop all running threads
            return


#test_04 for testing 404 Not Found status code
class test_06(unittest.TestCase):
    def runTest(self):    
        #Testing conditional get request
        try:
            headers = {
                'accept-Encoding': 'gzip;q=1.0,br;q=0.7',
                'Accept': '*/*',
                'Connection': 'keep-alive'
            }
            
            r = requests.head(url + "/xyz.js", headers = headers)
            if(r.status_code == 404):
                print('\nHEAD TEST-04 PASS', r.status_code, r.reason)
        except Exception as ex:
            print('\n\nHEAD TEST-04  FAIL', ex)

        finally:
            # Stop all running threads
            return




#test_01 for testing 204 status code
class test_07(unittest.TestCase):
    def runTest(self):    
        #Testing conditional get request
        try:
            data = dict(
            )
            r = requests.post(url + "/postdata.txt",data = data)
            if(r.status_code ==204):
                print('\nPOST TEST-01 PASS', r.status_code, r.reason)
        except Exception as ex:
            print('\n\nPOST TEST-01 FAIL', ex)

        finally:
            # Stop all running threads
            return

#test_01 for testing 204 status code
class test_08(unittest.TestCase):
    def runTest(self):    
        try:
            data = dict(
            )
            response = requests.put(url + "/PutFiles/third.txt",data = data)
            if(response.status_code ==204):
                print('\nPUT TEST-01 PASS', response.status_code, response.reason)
        except Exception as ex:
            print('\n\nPUT TEST-01 FAIL', ex)

        finally:
            # Stop all running threads
            return




#test_01 for testing 200 delete status code
class test_09(unittest.TestCase):
    def runTest(self):    
        try:
            response = requests.delete(url + "/PutFiles/third.txt")
            if(response.status_code ==204):
                print('\nDELETE TEST-01 PASS', response.status_code, response.reason)
        except Exception as ex:
            print('\n\nPUT TEST-01 FAIL', ex)

        finally:
            # Stop all running threads
            return




# #test_02 for testing 200 status code
# class test_09(unittest.TestCase):
#     def runTest(self):    
#         #Testing conditional get request
#         try:
#             data = dict(
#                 key1='TEST',
#                 value1='Muddayya'
#             )
#             print("I am here")
#             r = requests.post(url + "/postdata.txt",data = data)
#             print(r.status_code)
#             if(r.status_code == 200):
#                 print('\nPOST TEST-02 PASS', r.status_code, r.reason)
#         except Exception as ex:
#             print('\n\nPOST TEST-02 FAIL', ex)

#         finally:
#             # Stop all running threads
#             return

  


    

class test_10(unittest.TestCase):

    def runTest(self):
        try:
            data = dict(
                key1='TEST',
                value1='Muddayya'
            )
            print("I am here")
            r = requests.put(url + "/PutFiles/third.txt",
                data= data
            )
            print("not here")
            print(f"Status : {r.status_code} {r.reason}")
            print("Headers:", r.headers)
        except Exception as ex:
            print('Something went horribly wrong!', ex)
        finally:
            return















if __name__ == "__main__":

    unittest.main()

