#this file helps the request handlers in building the response.
import datetime
import time
import os
import secrets

def build_response(resDict):
    #this function takes resDict as arguments and then builds response from it.
    resLine = resDict['responseLine']['httpver'] + " "
    resLine += resDict['responseLine']['statusCode'] + " "
    resLine += resDict['responseLine']['phrase']

    response = resLine + "\r\n"
    headers = resDict['resheaders']
    Body = resDict['responseBody']

    for header in headers:
        response += header+": "+headers[header]+"\r\n"
    response += "\r\n"
    response = response.encode()
    response += Body

    return response

def Format_date(Req_date):
    dt = Req_date
    weekday = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"][dt.weekday()]
    month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][dt.month - 1]
    return "%s, %02d %s %04d %02d:%02d:%02d GMT" % (weekday, dt.day, month, dt.year, dt.hour, dt.minute, dt.second)


def get_Content_Encoding(accept_encoding):
    
    Cont_Encode = ''
    CompQ = 0.0
    if(accept_encoding == ""):
        Cont_Encode = 'Identity'
        return Cont_Encode

    encode_list = accept_encoding.split(',')
    if(len(encode_list) == 1):
        Cont_Encode = encode_list.split(';')[0]
        return Cont_Encode

    for CE in encode_list:
        tmp1 = CE.split(';')
        enType = tmp1[0]
        qval = float(tmp1[1].split('=')[1])
        if(qval > CompQ):
            Cont_Encode = enType
            CompQ = qval
    return Cont_Encode


def parseifmatch(ifmatch):
    return ifmatch.split(',')

def Bulid_cookie(cAddress):
    cookie_id = secrets.token_urlsafe(16)
    path = os.getcwd() +"/cookie.txt"
    cookie_log = str(cAddress[0])  +"   "+ str(cAddress[1])+":"+"        " +cookie_id
    with open(path, "a") as cookie:
        cookie.write(cookie_log)
        cookie.write("\n")
    return cookie_id


def givePhrase(statusCode):
	Phrases={
		"100" : "Continue",
		"101" : "Switching Protocols",
		"200" : "OK",
		"201" : "Created",
		"202" : "Accepted",
		"203" : "Non-Authoritative Information",
		"204" : "No Content",
		"205" : "Reset Content",
		"206" : "Partial Content",
		"300" : "Multiple Choices",
		"301" : "Moved Permanently",
		"302" : "Found",
		"303" : "See Other",
		"304" : "Not Modified",
		"305" : "Use Proxy",
		"307" : "Temporary Redirect",
		"400" : "Bad Request",
		"401" : "Unauthorized",
		"402" : "Payment Required",
		"403" : "Forbidden",
		"404" : "Not Found",
		"405" : "Method Not Allowed",
		"406" : "Not Acceptable",
		"407" : "Proxy Authentication Required",
		"408" : "Request Timeout",
		"409" : "Conflict",
		"410" : "Gone",
		"411" : "Length Required",
		"412" : "Precondition Failed",
		"413" : "Request Entity Too Large",
		"414" : "Request-URI Too Large",
		"415" : "Unsupported Media Type",
		"416" : "Requested range not satisfiable",
		"417" : "Expectation Failed",
		"500" : "Internal Server Error",
		"501" : "Not Implemented",
		"502" : "Bad Gateway",
		"503" : "Service Unavailable",
		"504" : "Gateway Time-out",
		"505" : "HTTP Version not supported"
	}
	return Phrases[str(statusCode)]


def get_content_ext(AcceptVal):
        
    content_ext = {

        
            "audio/aac"                     : ".aac"    ,
            "application/x-abiword"         : ".abw"	,
            "application/x-freearc"         : ".arc"	,
            "video/x-msvideo"               : ".avi"    ,
            "application/vnd.amazon.ebook"  : ".azw"    ,
            "application/octet-stream"      : ".bin"	,
            "image/bmp"                     : ".bmp"    ,
            "application/x-bzip"            : ".bz"	    ,
            "application/x-bzip2"           : ".bz2"    ,
            "application/x-csh"             : ".csh"    ,
            "text/css"                      : ".css"    ,
            "text/csv"                      : ".csv"	,
            "application/msword"            : ".doc"    ,
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document":".docx"	 ,
            "application/vnd.ms-fontobject" : ".eot"	,
            "application/epub+zip"          : ".epub"   ,
            "application/gzip"              : ".gz"	    ,
            "image/gif"                     : ".gif"	,
            "text/html"                     : ".html"   ,
            "text/plain"                    : ".txt"    , 
            "image/vnd.microsoft.icon"      : ".ico" 	,
            "text/calendar"                 : ".ics"	,
            "application/java-archive"      : ".jar"	,
            "image/jpeg"                    : ".jpeg"	,
            "text/javascript"               : ".js"	    ,
            "application/json"              : ".json"	,
            "application/ld+json"           : ".jsonld" ,
            "audio/midi"                    : ".mid"	,
            "audio.mpeg"                    : ".mp3"	,
            "video/mpeg"                    : ".mpeg"	,
            "application/vnd.apple.installer+xml":".mpkg"	 ,
            "application/vnd.oasis.opendocument.presentation"   :   ".odp"	 ,
            "application/vnd.oasis.opendocument.spreadsheet"    :   ".ods"	 ,
            "audio/ogg"                     : ".oga"	,
            "video/ogg"                     : ".ogv"	,
            "application/ogg"               : ".ogx"	,
            "font/otf"                      : ".otf"	,
            "image/png"                     : ".png"	,
            "application/pdf"               : ".pdf"	,
            "appliction/php"                : ".php"	,
            "application/vnd.ms-powerpoint" : ".ppt"	,
            "application/vnd.openxmlformats-officedocument.presentationml.presentation":".pptx"	 ,
            "application/x-rar-compressed"  : ".rar"	,
            "application/rtf"               : ".rtf"	,
            "application/x-sh"              : ".sh"	    ,
            "image/svg+xml"                 : ".svg"	,
            "application/x-shockwave-flash" : ".swf"	,
            "application/x-tar"             : ".tar"	,
            "image/tiff"                    : ".tif"	,
            "video/mp2t"                    : ".ts"	    ,
            "font/ttf"                      : ".ttf"	,
            "application/vnd.visio"         : ".vsd"	,
            "audio/wav"                     : ".wav"	,
            "audio/webm"                    : ".weba"	,
            "video/webm"                    : ".webm"	,
            ".webp"                         : ".webp"	,
            "font/woff"                     : ".woff"   ,
            "font/woff2"                    : ".woff2"  ,
            "application/xhtml+xml"         : ".xhtml"  ,
            "application/vnd.ms-excel"      : ".xls"	,
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":".xlsx"	 ,
            "application/xml"               : ".xml"	,
            "application/vnd.mozilla.xul+xml":".xul"    ,
            "application/zip"               : ".zip"	,
            "video/3gpp"                    : ".3gp"	,
            "video/3gpp2"                   : ".3g2"    ,
            "application/x-7z-compressed"   : ".7z"
    }

    return content_ext[AcceptVal]